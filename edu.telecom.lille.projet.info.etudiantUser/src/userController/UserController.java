package userController;


import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import userModel.Admin;
import userModel.Group;
import userModel.Student;
import userModel.Teacher;
import userModel.User;
import userModel.UserDB;
/**
 * Cette classe est le contrôleur d'utilisateurs que vous devez implémenter. 
 * Elle contient un attribut correspondant à la base de données utilisateurs que vous allez créer.
 * Elle contient toutes les fonctions de l'interface IUserController que vous devez implémenter.
 * 
 * @author Jose Mennesson (Mettre à jour)
 * @version 04/2016 (Mettre à jour)
 * 
 */

//TODO Classe à modifier

public class UserController implements IUserController
{
	
	/**
	 * Contient une instance de base de données d'utilisateurs
	 * 
	 */
	private UserDB userDB=null;
	
	
	/**
	 * Constructeur de controleur d'utilisateurs créant la base de données d'utilisateurs
	 * 
	 * @param userfile
	 * 		Fichier XML contenant la base de données d'utilisateurs
	 */
	public UserController(String userfile){
		UserDB userDB=new UserDB(userfile);
		this.setUserDB(userDB);
		this.loadDB();
	}

	@Override
	public String getUserName(String userLogin) {
		
		if (this.getUserDB().getUserByLogin(userLogin).equals(null)){
			return "Erreur";
		}
		String name = this.getUserDB().getUserByLogin(userLogin).getLastname()+" "+this.getUserDB().getUserByLogin(userLogin).getFirstname();
		
		
		return name;
	}

	@Override
	public String getUserClass(String userLogin, String userPwd) {
		User user = this.getUserDB().getUserByLogin(userLogin);
		if (user.equals(null)){
			return "";
		}
		if (user.getPassword().equals(userPwd)){
			if (user instanceof Student){
				return "Student";
			}
			if (user instanceof Teacher){
				return "Teacher";
			}
			if (user instanceof Admin){
				return "Administrator";
			}
		}
		
		return "";
	}

	@Override
	public int getStudentGroup(String studentLogin) {
		if (this.getUserDB().getUserByLogin(studentLogin) instanceof Student){
			return ((Student)this.getUserDB().getUserByLogin(studentLogin)).getGroupID();
		}
		return 0;
		
	}

	@Override
	public boolean addAdmin(String adminLogin, String newAdminlogin, int adminID, String firstname, String surname,
			String pwd) {
		Admin admin = new Admin(adminLogin, newAdminlogin, adminID, firstname, surname, pwd);
		this.userDB.addAdmin(admin);
		this.saveDB();
		return true;
	}

	@Override
	public boolean addTeacher(String adminLogin, String newteacherLogin, int teacherID, String firstname,
			String surname, String pwd) {
		Teacher teacher = new Teacher(adminLogin, newteacherLogin, teacherID, firstname, surname, pwd);
		this.userDB.addTeacher(teacher);
		this.saveDB();
		return true;
	}

	@Override
	public boolean addStudent(String adminLogin, String newStudentLogin, int studentID, String firstname,
			String surname, String pwd) {
		Student student = new Student(adminLogin, newStudentLogin, studentID, firstname, surname, pwd);
		this.userDB.addStudent(student);
		this.saveDB();
		return true;
	}

	@Override
	public boolean removeUser(String adminLogin, String userLogin) {
		for(int i=0;i<this.getUserDB().getUserList().size();i++){
			if(userLogin==this.getUserDB().getUserList().get(i).getLogin()){
				this.getUserDB().getUserList().remove(i);
				this.saveDB();
				return true;
			}
		}
		
		return false;
	}

	@Override
	public boolean addGroup(String adminLogin, int groupId) {
		for(int i=0;i<this.userDB.getGroups().size();i++){
			if(this.userDB.getGroups().get(i).getGroupID()==groupId){
				return false;
			}
		}
		Group group = new Group(adminLogin,groupId);	
		this.saveDB();
		return this.getUserDB().addGroup(group);
	}

	@Override
	public boolean removeGroup(String adminLogin, int groupId) {
		for(int i=0;i<this.getUserDB().getGroups().size();i++){
			if(this.getUserDB().getGroups().get(i).getGroupID()==groupId){
				this.getUserDB().getGroups().remove(i);
				for(int j=0;j<this.getUserDB().getUserList().size();j++){
					
					if(this.getUserDB().getUserList().get(j) instanceof Student){
						
						if(((Student) this.getUserDB().getUserList().get(j)).getGroupID()==groupId){
							
							((Student)this.getUserDB().getUserList().get(j)).setGroupID(0);
							
						}
					}					
				}
				this.saveDB();
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean associateStudToGroup(String adminLogin, String studentLogin, int groupId) {
		
		User student = this.getUserDB().getUserByLogin(studentLogin);
		if(student.equals(null)){
			return false;
		}
		if(student instanceof Student){
			((Student)student).setGroupID(groupId);
		}
		for(int j=0;j<this.userDB.getGroups().size();j++){
			if(this.getUserDB().getGroups().get(j).getGroupID()==groupId){
				this.getUserDB().getGroups().get(j).getStudents().add((Student) student);
				return true;
			}		
		}
		Group group = new Group(adminLogin, groupId);
		group.getStudents().add((Student) this.getUserDB().getUserByLogin(studentLogin));
		this.getUserDB().addGroup(group);
		this.saveDB();
		return true;
	}


	
	@Override
	public String[] usersToString() {
		
		String[] usersList = new String[this.getUserDB().getUserList().size()];
		for (int i=0;i<this.getUserDB().getUserList().size();i++){
			if(this.getUserDB().getUserList().get(i) instanceof Teacher){
				usersList[i]=("User : Teacher - Login : "+this.getUserDB().getUserList().get(i).getLogin()+" - Pr�nom : "+this.getUserDB().getUserList().get(i).getFirstname()+" - Nom : "+this.getUserDB().getUserList().get(i).getLastname()+" - Mot de passe : "+this.getUserDB().getUserList().get(i).getPassword()+" - ID : "+this.getUserDB().getUserList().get(i).getUserID());
			}
			if(this.getUserDB().getUserList().get(i) instanceof Admin){
				usersList[i]=("User : Admin - Login : "+this.getUserDB().getUserList().get(i).getLogin()+" - Pr�nom : "+this.getUserDB().getUserList().get(i).getFirstname()+" - Nom : "+this.getUserDB().getUserList().get(i).getLastname()+" - Mot de passe : "+this.getUserDB().getUserList().get(i).getPassword()+" - ID : "+this.getUserDB().getUserList().get(i).getUserID());
			}
			if(this.getUserDB().getUserList().get(i) instanceof Student){
				usersList[i]=("User : Student - Login : "+this.getUserDB().getUserList().get(i).getLogin()+" - Pr�nom : "+this.getUserDB().getUserList().get(i).getFirstname()+" - Nom : "+this.getUserDB().getUserList().get(i).getLastname()+" - Mot de passe : "+this.getUserDB().getUserList().get(i).getPassword()+"- ID : "+this.getUserDB().getUserList().get(i).getUserID()+" - GroupID : "+((Student)this.getUserDB().getUserList().get(i)).getGroupID());
			}
		}
		
		return usersList;
	}

	@Override
	public String[] usersLoginToString() {
		String[] loginsList = new String[this.getUserDB().getUserList().size()];
		for (int i=0;i<this.getUserDB().getUserList().size();i++){
			loginsList[i]=this.getUserDB().getUserList().get(i).getLogin();
		}
		return loginsList;
	}

	@Override
	public String[] studentsLoginToString() {
		String[] loginsList = new String[this.getUserDB().getUserList().size()];
		int j =0;
		for (int i=0;i<this.getUserDB().getUserList().size();i++){
			System.out.println(this.getUserDB().getUserList().get(i).getClass());
			if(this.getUserDB().getUserList().get(i) instanceof Student){
				
				loginsList[j]=this.getUserDB().getUserList().get(i).getLogin();
				j++;
				
			}
		}
		
		return loginsList;
	}

	@Override
	public String[] groupsIdToString() {
		String[] idList = new String[this.getUserDB().getGroups().size()];
		for (int i=0;i<this.getUserDB().getGroups().size();i++){
			if(this.getUserDB().getGroups().get(i).getGroupID()!=0){
				idList[i]=Integer.toString(this.getUserDB().getGroups().get(i).getGroupID());
			}
			
			
		}
		
		return idList;
	}

	@Override
	public String[] groupsToString() {
		
		ArrayList<Group> groups = this.getUserDB().getGroups();
		String[] infosList = new String[groups.size()];
		
		for (int i=0;i<groups.size();i++){
			if(groups.get(i).getGroupID()!=0){
				infosList[i] = "GroupID : "+Integer.toString(groups.get(i).getGroupID())+" - AdminLogin : "+groups.get(i).getAdminLogin()+" - StudentNB : "+groups.get(i).getStudentNb()+" - Students : ";
				for (int j=0;j<groups.get(i).getStudents().size();j++){
					if(groups.get(i).getStudents().get(i) instanceof Student){
						infosList[i]=infosList[i]+" - Student : - Login : "+groups.get(i).getStudents().get(j).getLogin()+" - Pr�nom : "+groups.get(i).getStudents().get(j).getFirstname()+" - Nom : "+groups.get(i).getStudents().get(j).getLastname()+" - Mot de passe : "+groups.get(i).getStudents().get(j).getPassword()+"- ID : "+groups.get(i).getStudents().get(j).getUserID()+" - GroupID : "+((Student)groups.get(i).getStudents().get(j)).getGroupID();
				}
			}
			
			}
		}
			
		return infosList;
	}

	@Override
	public boolean loadDB() {
		
		final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		try {

		    final DocumentBuilder builder = factory.newDocumentBuilder();       

		    final Document document= builder.parse(this.getUserDB().getFile());
		    
		    final Element racine = document.getDocumentElement();
		   
		    final NodeList racineNoeuds = racine.getChildNodes();
		    
		    
		    
		    
		    final NodeList adminList = ((Element) racineNoeuds).getElementsByTagName("Administrator");
		    final NodeList teacherList = ((Element) racineNoeuds).getElementsByTagName("Teacher");
		    final NodeList studentList = ((Element) racineNoeuds).getElementsByTagName("Student");
		   
		   
		    	
		        
		    for (int i = 0; i<adminList.getLength();i++){
		    	NodeList admin = adminList.item(i).getChildNodes();
			    Admin newadmin = new Admin("su",admin.item(1).getTextContent(),Integer.parseInt(admin.item(9).getTextContent()),admin.item(3).getTextContent(),admin.item(5).getTextContent(),admin.item(7).getTextContent());
			    this.getUserDB().addAdmin(newadmin);
		    }
		   
		    for (int i = 0; i<teacherList.getLength();i++){
		    	NodeList teacher = teacherList.item(i).getChildNodes();
			    Teacher newteacher = new Teacher("su",teacher.item(1).getTextContent(),Integer.parseInt(teacher.item(9).getTextContent()),teacher.item(3).getTextContent(),teacher.item(5).getTextContent(),teacher.item(7).getTextContent());
			    this.getUserDB().addTeacher(newteacher);
		    }
		   
		    for (int i = 0; i<studentList.getLength();i++){
		    	NodeList student = studentList.item(i).getChildNodes();
			    Student newstudent = new Student("su",student.item(1).getTextContent(),Integer.parseInt(student.item(9).getTextContent()),student.item(3).getTextContent(),student.item(5).getTextContent(),student.item(7).getTextContent());
			    this.getUserDB().addStudent(newstudent);
			    this.associateStudToGroup("su", student.item(1).getTextContent(), Integer.parseInt(student.item(11).getTextContent()));
			    
		    }
		    
		       
		  

		}

		catch (final ParserConfigurationException e) {

		    e.printStackTrace();
		    return false;

		}

		catch (final SAXException e) {

		    e.printStackTrace();
		    return false;

		}

		catch (final IOException e) {

		    e.printStackTrace();
		    return false;

		}
		

		return true;
	}

	@Override
	public boolean saveDB() {
		
		final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		
		try {
		final DocumentBuilder builder = factory.newDocumentBuilder();       

		final Document document= builder.newDocument();
	    
	    final Element racine = document.createElement("UsersDB");
	    
	    document.appendChild(racine);
	    
	    final Element Groups = document.createElement("Groups");
	    racine.appendChild(Groups);
	    
	    final Element Students = document.createElement("Students");
	    racine.appendChild(Students);
	    
	    final Element Teachers = document.createElement("Teachers");
	    racine.appendChild(Teachers);
	    
	    final Element Administrators = document.createElement("Administrators");
	    racine.appendChild(Administrators);
	    
	    
	    for(int i = 0;i<this.getUserDB().getGroups().size();i++){
	    	
	    	if(this.getUserDB().getGroups().get(i) instanceof Group){
	    		
	    		final Element group = document.createElement("Group");
	    		final Element groupId = document.createElement("groupId");
	    		
	    		groupId.setTextContent(Integer.toString(this.getUserDB().getGroups().get(i).getGroupID()));
	    		
	    		group.appendChild(groupId);
	    		Groups.appendChild(group);
	    		
	    		
	    	}
	    	
	    }
	    
	    
	    
	    for(int i = 0;i<this.getUserDB().getUserList().size();i++){
	    	
	    	
	    	if(this.getUserDB().getUserList().get(i) instanceof Student){
	    		
	    		final Element student = document.createElement("Student");
	    		final Element login = document.createElement("login");
	    		final Element firstname = document.createElement("firstname");
	    		final Element surname = document.createElement("surname");
	    		final Element pwd = document.createElement("pwd");
	    		final Element studentId = document.createElement("studentId");
	    		final Element groupId = document.createElement("groupId");
	    		
	    		
	    		login.setTextContent(this.getUserDB().getUserList().get(i).getLogin());
	    		firstname.setTextContent(this.getUserDB().getUserList().get(i).getFirstname());
	    		surname.setTextContent(this.getUserDB().getUserList().get(i).getLastname());
	    		pwd.setTextContent(this.getUserDB().getUserList().get(i).getPassword());
	    		studentId.setTextContent(Integer.toString(this.getUserDB().getUserList().get(i).getUserID()));
	    		groupId.setTextContent(Integer.toString(((Student)this.getUserDB().getUserList().get(i)).getGroupID()));
	    	
	    		student.appendChild(login);
	    		student.appendChild(firstname);
	    		student.appendChild(surname);
	    		student.appendChild(pwd);
	    		student.appendChild(studentId);
	    		student.appendChild(groupId);
	    		
	    		Students.appendChild(student);
	    
	    	}
	    	if(this.getUserDB().getUserList().get(i) instanceof Teacher){
	    		
	    		final Element teacher = document.createElement("Teacher");
	    		final Element login = document.createElement("login");
	    		final Element firstname = document.createElement("firstname");
	    		final Element surname = document.createElement("surname");
	    		final Element pwd = document.createElement("pwd");
	    		final Element teacherId = document.createElement("teacherId");
	    		
	    		
	    		login.setTextContent(this.getUserDB().getUserList().get(i).getLogin());
	    		firstname.setTextContent(this.getUserDB().getUserList().get(i).getFirstname());
	    		surname.setTextContent(this.getUserDB().getUserList().get(i).getLastname());
	    		pwd.setTextContent(this.getUserDB().getUserList().get(i).getPassword());
	    		teacherId.setTextContent(Integer.toString(this.getUserDB().getUserList().get(i).getUserID()));
	    	
	    		teacher.appendChild(login);
	    		teacher.appendChild(firstname);
	    		teacher.appendChild(surname);
	    		teacher.appendChild(pwd);
	    		teacher.appendChild(teacherId);
	    		
	    		Teachers.appendChild(teacher);
	    		
	    	}
	    	if(this.getUserDB().getUserList().get(i) instanceof Admin){
	    		
	    		final Element admin = document.createElement("Administrator");
	    		final Element login = document.createElement("login");
	    		final Element firstname = document.createElement("firstname");
	    		final Element surname = document.createElement("surname");
	    		final Element pwd = document.createElement("pwd");
	    		final Element teacherId = document.createElement("adminId");
	    		
	    		
	    		login.setTextContent(this.getUserDB().getUserList().get(i).getLogin());
	    		firstname.setTextContent(this.getUserDB().getUserList().get(i).getFirstname());
	    		surname.setTextContent(this.getUserDB().getUserList().get(i).getLastname());
	    		pwd.setTextContent(this.getUserDB().getUserList().get(i).getPassword());
	    		teacherId.setTextContent(Integer.toString(this.getUserDB().getUserList().get(i).getUserID()));
	    	
	    		admin.appendChild(login);
	    		admin.appendChild(firstname);
	    		admin.appendChild(surname);
	    		admin.appendChild(pwd);
	    		admin.appendChild(teacherId);
	    		
	    		Administrators.appendChild(admin);
	    		
	    	}
	    }
	    
	    
	    
	    final TransformerFactory transformerFactory = TransformerFactory.newInstance();
	    final Transformer transformer = transformerFactory.newTransformer();
	    final DOMSource source = new DOMSource(document);
	    final StreamResult sortie = new StreamResult(this.getUserDB().getFile());
			
	
	    transformer.setOutputProperty(OutputKeys.VERSION, "1.0");
	    transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
	    transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");			
	    		
	
	    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	    transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			

	    transformer.transform(source, sortie);

	    
	    
	    
		}
		
		catch (final ParserConfigurationException e) {

		    e.printStackTrace();
		    return false;

		}

		catch (TransformerConfigurationException e) {
		    e.printStackTrace();
		    return false;
		}
		
		catch (TransformerException e) {
		    e.printStackTrace();
		    return false;
		}
	    
		return true;
	}

	public UserDB getUserDB() {
		return userDB;
	}

	public void setUserDB(UserDB userDB) {
		this.userDB = userDB;
	}
	
	
	
}

