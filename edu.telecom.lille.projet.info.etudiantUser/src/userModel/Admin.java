/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/
package userModel;

import userModel.User;
// Start of user code (user defined imports)

// End of user code

/**
 * Description of Admin.
 * 
<<<<<<< HEAD
 * @author elaforest & ggresillion
=======
 * @author elaforest
>>>>>>> origin/master
 */
public class Admin extends User {
	/**
	 * Description of the property adminID.
	 */
	private int ID = 0;

	// Start of user code (user defined attributes for Admin)

	// End of user code

	/**
	 * The constructor.
	 */
	public Admin(String adminLogin, String newadminLogin,int ID, String firstname, String surname, String pwd) {
		// Start of user code constructor for Admin)
		super(adminLogin,newadminLogin,ID,firstname,surname,pwd);
		this.ID=ID;
		// End of user code
	}

	/**
	 * Description of the method addStudent.
	 */
	public void addStudent() {
		// Start of user code for method addStudent
		// End of user code
	}

	/**
	 * Description of the method addTeacher.
	 */
	public void addTeacher() {
		// Start of user code for method addTeacher
		// End of user code
	}

	/**
	 * Description of the method associateStudToGroup.
	 */
	public void associateStudToGroup() {
		// Start of user code for method associateStudToGroup
		// End of user code
	}

	/**
	 * Description of the method addGroup.
	 */
	public void addGroup() {
		// Start of user code for method addGroup
		// End of user code
	}

	// Start of user code (user defined methods for Admin)

	// End of user code
	/**
	 * Returns adminID.
	 * @return adminID 
	 */
	public int getID() {
		return this.ID;
	}

}
