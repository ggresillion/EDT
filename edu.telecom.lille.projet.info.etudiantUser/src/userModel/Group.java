/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/
package userModel;

import java.util.ArrayList;

// Start of user code (user defined imports)

// End of user code

/**
 * Description of Group.
 * 
 * @author elaforest
 */
public class Group {
	
	/**
	 * Description of the property groupID.
	 */
	private String adminLogin = "";
	/**
	 * Description of the property groupID.
	 */
	private int groupID = 0;

	/**
	 * Description of the property studentNb.
	 */
	private int studentNb = 0;

	/**
	 * Description of the property Student.
	 */
	public ArrayList<Student> Student = new ArrayList<Student>();

	// Start of user code (user defined attributes for Group)

	// End of user code

	/**
	 * The constructor.
	 */
	public Group(String adminLogin, int groupId) {
		// Start of user code constructor for Group)
		super();
		this.adminLogin=adminLogin;
		this.groupID=groupId;
		this.studentNb=this.getStudentNb();
		
		// End of user code
	}

	// Start of user code (user defined methods for Group)

	// End of user code
	
	
	
	//set et get
	
	/**
	 * Returns groupID.
	 * @return groupID 
	 */
	public int getGroupID() {
		return this.groupID;
	}

	/**
	 * Sets a value to attribute groupID. 
	 * @param newGroupID 
	 */
	public void setGroupID(int newGroupID) {
		this.groupID = newGroupID;
	}

	/**
	 * Returns studentNb.
	 * @return studentNb 
	 */
	public int getStudentNb() {
		this.studentNb=this.getStudents().size();
		return studentNb;
	}

	/**
	 * Sets a value to attribute studentNb. 
	 * @param newStudentNb 
	 */
	public void setStudentNb() {
		this.studentNb = this.getStudents().size();
	}

	/**
	 * Returns Student.
	 * @return Student 
	 */
	public ArrayList<Student> getStudents() {
		return this.Student;
		
	}

	/**
	 * @return the adminLogin
	 */
	public String getAdminLogin() {
		return adminLogin;
	}

	/**
	 * @param adminLogin the adminLogin to set
	 */
	public void setAdminLogin(String adminLogin) {
		this.adminLogin = adminLogin;
	}

}
