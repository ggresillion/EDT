package userModel;

import java.util.ArrayList;

/**
 * 
 * Cette classe gére la base de données d'utilisateurs. Elle doit permettre de sauvegarder et charger les utilisateurs et les groupes à partir d'un fichier XML. 
 * La structure du fichier XML devra être la même que celle du fichier userDB.xml.
 * @see <a href="../../userDB.xml">userDB.xml</a> 
 * 
 * @author Jose Mennesson (Mettre à jour)
 * @version 04/2016 (Mettre à jour)
 * 
 */



public class UserDB {
	
	/**
	 * 
	 * Le fichier contenant la base de données.
	 * 
	 */
	private String file;
	private ArrayList<User> userList;
	private ArrayList<Group> groupList;
	
	
	/**
	 * 
	 * Constructeur de UserDB. 
	 * 
	 * !!!!!!!!!!!! PENSEZ À AJOUTER UN ADMINISTRATEUR (su par exemple) QUI VOUS PERMETTRA DE CHARGER LA BASE DE DONNÉES AU DEMARRAGE DE L'APPLICATION !!!!!!
	 * 
	 * @param file
	 * 		Le nom du fichier qui contient la base de données.
	 */
	public UserDB(String file){
		
		super();
		this.setFile(file);
		
		this.userList= new ArrayList<User>();
		Admin admin = new Admin("su", "su", 0, "su", "su", "superUser");
		this.userList.add(admin);
		this.groupList=new ArrayList<Group>();
	}
	
	public boolean addAdmin (Admin admin){
		
		if(this.isUserRegistered(admin)==false){
			
		this.userList.add(admin);
		
		return true;
		
		}
		
		return false;
	}

	public boolean addTeacher (Teacher teacher){
		
		if(this.isUserRegistered(teacher)==false){
			
		this.userList.add(teacher);
		
		return true;
		
		}
		
		return false;
	}
	
	
	public boolean addStudent (Student student){
		
		if(this.isUserRegistered(student)==false){
			
		this.userList.add(student);
			
		return true;
		
		}
		
		return false;
	}
	

	public boolean isUserRegistered (User user){
		
		
			for(int i=0;i<userList.size();i++){
				
				if(user.getLogin().equals(userList.get(i).getLogin())|| user.getUserID()==userList.get(i).getUserID()){
					
					return true;
				}
			}
		
		
			
		
		
		return false;
	}

	public boolean addGroup (Group group) {
		if(!this.getGroups().contains(group)){
			this.getGroups().add(group);
			return true;
		}
		return false;
	}
	
	
	public ArrayList<Group> getGroups(){
		return this.groupList;
	}
	
	public User getUserByLogin(String login){
		
		for(int i=0;i<this.getUserList().size();i++){
			
			
			if(login.equals(this.getUserList().get(i).getLogin())){
				return this.getUserList().get(i);
			}
		}

			
		return null;
	
	}

	public ArrayList<User> getUserList() {
		return userList;
	}


	/**
	 * Getter de file
	 * 
	 * @return 
	 * 		Le nom du fichier qui contient la base de données.
	 */
	
	public String getFile() {
		return file;
	}

	public void setUserList(ArrayList<User> userList) {
		this.userList = userList;
	}


	/**
	 * Setter de file
	 * 
	 * @param file
	 * 		Le nom du fichier qui contient la base de données.
	 */
	
	public void setFile(String file) {
		this.file = file;
	}
	
	
	
}

