/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/
package userModel;

import java.util.Date;
// Start of user code (user defined imports)

// End of user code

/**
 * Description of TimeConstraint.
 * 
 * @author elaforest
 */
public class TimeConstraint {
	/**
	 * Description of the property identifiant.
	 */
	private int identifiant = 0;

	/**
	 * Description of the property login.
	 */
	private String login = "";

	/**
	 * Description of the property startingDate.
	 */
	private Date startingDate = new Date();

	/**
	 * Description of the property endingDate.
	 */
	private Date endingDate = new Date();

	/**
	 * Description of the property comment.
	 */
	private String comment = "";

	// Start of user code (user defined attributes for TimeConstraint)

	// End of user code

	/**
	 * The constructor.
	 */
	public TimeConstraint() {
		// Start of user code constructor for TimeConstraint)
		super();
		// End of user code
	}

	// Start of user code (user defined methods for TimeConstraint)

	// End of user code
	/**
	 * Returns identifiant.
	 * @return identifiant 
	 */
	public int getIdentifiant() {
		return this.identifiant;
	}

	/**
	 * Sets a value to attribute identifiant. 
	 * @param newIdentifiant 
	 */
	public void setIdentifiant(int newIdentifiant) {
		this.identifiant = newIdentifiant;
	}

	/**
	 * Returns login.
	 * @return login 
	 */
	public String getLogin() {
		return this.login;
	}

	/**
	 * Sets a value to attribute login. 
	 * @param newLogin 
	 */
	public void setLogin(String newLogin) {
		this.login = newLogin;
	}

	/**
	 * Returns startingDate.
	 * @return startingDate 
	 */
	public Date getStartingDate() {
		return this.startingDate;
	}

	/**
	 * Sets a value to attribute startingDate. 
	 * @param newStartingDate 
	 */
	public void setStartingDate(Date newStartingDate) {
		this.startingDate = newStartingDate;
	}

	/**
	 * Returns endingDate.
	 * @return endingDate 
	 */
	public Date getEndingDate() {
		return this.endingDate;
	}

	/**
	 * Sets a value to attribute endingDate. 
	 * @param newEndingDate 
	 */
	public void setEndingDate(Date newEndingDate) {
		this.endingDate = newEndingDate;
	}

	/**
	 * Returns comment.
	 * @return comment 
	 */
	public String getComment() {
		return this.comment;
	}

	/**
	 * Sets a value to attribute comment. 
	 * @param newComment 
	 */
	public void setComment(String newComment) {
		this.comment = newComment;
	}

}
