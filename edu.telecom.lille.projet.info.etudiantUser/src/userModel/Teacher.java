/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/
package userModel;

import userModel.User;
// Start of user code (user defined imports)

// End of user code

/**
 * Description of Teacher.
 * 
 * @author elaforest
 */
public class Teacher extends User {
	/**
	 * Description of the property teacherID.
	 */


	// Start of user code (user defined attributes for Teacher)

	// End of user code

	/**
	 * The constructor.
	 */
	public Teacher(String adminLogin, String newteacherLogin,int ID, String firstname, String surname, String pwd) {
		// Start of user code constructor for Teacher)
		super(adminLogin,newteacherLogin,ID,firstname,surname,pwd);
		// End of user code
	}

	/**
	 * Description of the method createConstraint.
	 */
	public void createConstraint() {
		// Start of user code for method createConstraint
		// End of user code
	}

	// Start of user code (user defined methods for Teacher)

	// End of user code

}