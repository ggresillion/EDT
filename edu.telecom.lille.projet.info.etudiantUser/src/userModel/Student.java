/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/
package userModel;

import userModel.User;
// Start of user code (user defined imports)

// End of user code

/**
 * Description of Student.
 * 
 * @author elaforest
 */
public class Student extends User {
	/**
	 * Description of the property ID.
	 */


	/**
	 * Description of the property groupID.
	 */
	private int groupID = 0;

	// Start of user code (user defined attributes for Student)

	// End of user code

	/**
	 * The constructor.
	 */
	public Student(String adminLogin, String newStudentLogin,int ID, String firstname, String surname, String pwd) {
		// Start of user code constructor for Student)
		super(adminLogin,newStudentLogin,ID,firstname,surname,pwd);
	
		this.groupID=0;
		// End of user code
	}

	// Start of user code (user defined methods for Student)

	// End of user code


	/**
	 * Returns groupID.
	 * @return groupID 
	 */
	public int getGroupID() {
		return this.groupID;
	}

	/**
	 * Sets a value to attribute groupID. 
	 * @param newGroupID 
	 */
	public void setGroupID(int newGroupID) {
		this.groupID = newGroupID;
	}

}
