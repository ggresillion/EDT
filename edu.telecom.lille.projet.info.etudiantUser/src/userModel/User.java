/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/
package userModel;

// Start of user code (user defined imports)

// End of user code

/**
 * Classe contenant toutes les informations d'un utilisateur.
 * 
 * @author elaforest
 */
public class User {
	
	// Start of user code (user defined attributes for User)
	
	/**
	 * Login de l'administrateur qui a cr�� cet utilisateur.
	 */
	private String adminLogin = "";
	/**
	 * Login de l'utilisateur.
	 */
	private String login = "";

	/**
	 * Pr�nom de l'utilisateur.
	 */
	private String lastname = "";

	/**
	 * Nom de l'utilisateur.
	 */
	private String firstname = "";

	/**
	 * Mot de passe de l'utilisateur.
	 */
	private String password = "";
	
	private int ID=0;

	

	// End of user code

	/**
	 * The constructor.
	 */
	public User(String adminLogin, String login,int ID, String firstname, String surname, String pwd) {
		// Start of user code constructor for User)
		super();
		this.adminLogin=adminLogin;
		this.login=login;
		this.lastname=surname;
		this.firstname=firstname;
		this.password=pwd;
		this.ID=ID;
		
		// End of user code
	}

	// Start of user code (user defined methods for User)
	
	

	// End of user code
	
	
	
	
	
	//set et get des variables
	
	/**
	 * Returns login.
	 * @return login 
	 */
	public String getLogin() {
		return this.login;
	}

	/**
	 * Sets a value to attribute login. 
	 * @param newLogin 
	 */
	public void setLogin(String newLogin) {
		this.login = newLogin;
	}

	/**
	 * Returns lastname.
	 * @return lastname 
	 */
	public String getLastname() {
		return this.lastname;
	}

	/**
	 * Sets a value to attribute lastname. 
	 * @param newLastname 
	 */
	public void setLastname(String newLastname) {
		this.lastname = newLastname;
	}

	/**
	 * Returns firstname.
	 * @return firstname 
	 */
	public String getFirstname() {
		return this.firstname;
	}

	/**
	 * Sets a value to attribute firstname. 
	 * @param newFirstname 
	 */
	public void setFirstname(String newFirstname) {
		this.firstname = newFirstname;
	}

	/**
	 * Returns password.
	 * @return password 
	 */
	public String getPassword() {
		return this.password;
	}

	/**
	 * Sets a value to attribute password. 
	 * @param newPassword 
	 */
	public void setPassword(String newPassword) {
		this.password = newPassword;
	}

	public String getAdminLogin() {
		return adminLogin;
	}

	public void setAdminLogin(String adminLogin) {
		this.adminLogin = adminLogin;
	}

	public int getUserID() {	
		return this.ID;
	}

}
